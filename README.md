## my-common-config

- the idea here is:
  - support a full futured and fancy IPv6 setup accessible for normal users
  - full deb/apt integration, addon packages should be as simple as `apt-get install` or `dpkg-reconfigure`
  - native IPv6 fully routed
  - fully automated on mass kickstarts from tools like Open Nebula
  - fully automated on mass kickstarts on physical high end devices: hypervisors
  - simple user guided kickstart (and potentially automated too) on small SOHO setups like a Bpi R2
  - all of these be as similar and consistend as possible
  - developers should be able to run and test their code at home and find the exact same setup in prod/hosted environment
  - privacy in mind. one click services like email back in your hand


this package contains the very basic tools to have a debian linux fully functioning on a IPv6 network
it includes:

- dhclient config to better support DDNS and IPv6. i.e. send fqdn (ipv4 had hostname option for this and was default)
- dhclient plugin/scripts to make DNS and NTP servers work in IPv6 only
- sysctl settings to accept RA even when in forwarding mode and also accept static route advertisments from other VMs in the cluster
  - this is mostly needed for docker or other type of subnet-routing on a VM
- collection of simple bash tools, see manpages for more details


key tools:

- central simple plain config based off of bash vars. with potential debconf integration for simply kickstart and remote generation
- central library providing low level network helpers
- netconf-gen: generate system configs based off of configfile
- netconf-apply: apply full network setup without reboot


