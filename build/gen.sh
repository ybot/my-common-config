#!/bin/bash
set -e
: ${MY_DEBNAME="${PWD##*/}"}
: ${MY_HOST:="bastion.mitico"}
: ${MY_UPLOAD_PATH:="/var/www/html/repo/dev/pool/${MY_DEBNAME}"}
: ${MY_VERSION_PREFIX:="2.0."}

echo -en "$MY_DEBNAME (${MY_VERSION_PREFIX}$(TZ=UTC date +%Y.%m.%d.%H.%M)) unstable; urgency=low\n\n$(git log --format='  * %s')\n\n -- debbuilder <debuilder@email>  $(TZ=UTC date -R)" >debian/changelog
dpkg-buildpackage --no-sign

my_files="$(find ../ -maxdepth 1 -type f -name ${MY_DEBNAME}_${MY_VERSION_PREFIX}*)"

[ "$1" == "dry-run" ] && echo "## dry-run, only building no syncing..." && exit 0

rsync -Pi ${my_files} ${MY_HOST}:${MY_UPLOAD_PATH}
rm ${my_files}

[ "$1" == "no-publish" ] && echo "## no publish mode, synced packages, but not updating indicies" && exit 0

ssh ${MY_HOST} "${MY_UPLOAD_PATH}/../../publish.sh"
