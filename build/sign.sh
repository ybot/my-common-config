#!/bin/bash
set -Eeuo pipefail

: ${MY_HOST:="bastion.mitico"}
: ${MY_UPLOAD_PATH:="${MY_HOST}:/var/www/html/repo-incoming"}

#my_name="$(git log --format=%an HEAD~1..HEAD)"
#my_email="$(git log --format=%ae HEAD~1..HEAD)"
my_name="$(git config --get user.name)"
my_email="$(git config --get user.email)"

for file in $@; do
	scp ${MY_UPLOAD_PATH}/${file} /tmp/${file}
	gpg --clear-sign --default-key ${my_email} /tmp/${file}
	scp /tmp/${file}.asc ${MY_UPLOAD_PATH}/${file}
done
