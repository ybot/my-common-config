# how to setup a FOU udp tunnel encapsolation for SIT IPv6 tunnels for full NAT traversal support


## set variables


```
TUNNELBROKER_IP=<tunnelbroker ipv4>
CLIENT_IP=<client public IPv4>
UDP_PORT=<udp_port>
UPLINK_IF=<tunnelbroker uplink interface>
TUNNEL_SUBNET=<2001:aaaa:bbbb:cccc>		## the prefix part of a /64-ipv6 the code below appends the ::1/64 and ::2/64 as appropriate
TUNNEL_IF_NAME=<linux interface name>
```



## client setup



```
sudo modprobe fou
sudo ip fou add port ${UDP_PORT} ipproto 41 peer ${TUNNELBROKER_IP} peer_port ${UDP_PORT}
sudo ip link add name ${TUNNEL_IF_NAME} type sit remote ${TUNNELBROKER_IP} ttl 255 encap fou encap-sport ${UDP_PORT} encap-dport ${UDP_PORT}
sudo ip link set up ${TUNNEL_IF_NAME}
sudo ip addr add ${TUNNEL_SUBNET}::2/64 dev ${TUNNEL_IF_NAME}
sudo ip route add ::/0 dev ${TUNNEL_IF_NAME}
ping ${TUNNEL_SUBNET}::1
```



## tunnelbroker setup

```
## dynamically determine client PORT on connection setup, this is needs to be done better but for the sake of an exmaple I just used tcpdump
CLIENT_PORT=$(tcpdump -nni ${UPLINK_IF} -q -c 1 host ${CLIENT_IP} and udp port ${UDP_PORT} 2>/dev/null | awk '{ print $3 }' | sed 's/.*\.//')

## and fire up the tunnel
modprobe fou
ip fou add port ${UDP_PORT} ipproto 41 local ${TUNNELBROKER_IP}
ip link add name ${TUNNEL_IF_NAME} type sit remote ${CLIENT_IP} local ${TUNNELBROKER_IP} ttl 255 encap fou encap-sport ${UDP_PORT} encap-dport ${CLIENT_PORT}
ip link set up dev ${TUNNEL_IF_NAME}
ip addr add ${TUNNEL_SUBNET}::1/64 dev ${TUNNEL_IF_NAME}
```





# cleanup / delete interfaces

### tunnelbroker
```
ip link del ${TUNNEL_IF_NAME}
ip fou del port ${UDP_PORT} ipproto 41 local ${TUNNELBROKER_IP}
```

### client
```
sudo ip link del ${TUNNEL_IF_NAME}
sudo ip fou del port ${UDP_PORT} ipproto 41  local $(ip fou show | awk '{ print $6 }') peer ${TUNNELBROKER_IP} peer_port ${UDP_PORT}
```


