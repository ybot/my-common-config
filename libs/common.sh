#!/bin/bash
# this should be part of the importing script (if desired) so the libs can be used for non critical things as well - since these settings are inherited..
#set -Eeuo pipefail

## some defaults
: ${MY_NAMESERVERS_IPV4:="1.1.1.1 1.0.0.1"}
: ${MY_NAMESERVERS_IPV6:="2606:4700:4700::1111 2606:4700:4700::1001"}
#: ${MY_NAMESERVERS_DNS64:="2606:4700:4700::64 2606:4700:4700::6400"}		## cloudflare dns64 - cloudflare unfortunately is not as stable as google
: ${MY_NAMESERVERS_DNS64:="2001:4860:4860::6464 2001:4860:4860::64"}		## google dns64

: ${MY_NAT64_SUBNET:="64:ff9b::/96"}
: ${MY_NAT64_IPV6:="${MY_NAT64_SUBNET}"}
: ${MY_NAT64_IPV4:="192.168.255.0/24"}
: ${MY_VPN_IPV4:="192.168.254.0/24"}
: ${MY_DHCP6_PLEN:="96"}							## anything != 96 is highly experimental. its used in dhcp6 config generation for dhcppd and subnet
: ${MY_IPV6_MAX_PLEN_DELTA:="16"}					## max delta for ipv6 subnet calculations. if this is too big CPU and proc time may be very long
: ${MY_DHCPPD_STATE:="/var/lib/dhcp/dhclient6.prefixes"}			## dhcp prefix delegation state
: ${MY_DHCPPD_LOCAL_PLEN:="112"}					## allocated prefix from dhcp is further split locally into this micro size. this way we can host docker, podman and others without issues side-by-side
: ${MY_DHCPPD_LOCAL_DOCKER:="1"}					## by default use the first local micro-net for docker - get_subnet6_n is used for this
: ${MY_DHCPPD_LOCAL_PODMAN:="2"}					## by default use the second local micro-net for podman - get_subnet6_n is used for this
: ${MY_DHCPPD_LOCAL_KUBELET:="3"}					## by default use the third local micro-net for kubelet - get_subnet6_n is used for this
: ${MY_MGMTVRF:="mgmtvrf"}
: ${MY_MGMTVRF_ID:="1001"}
: ${MY_CEPH_CLUSTER:="ceph"}
: ${MY_CEPH_DC:="mydc"}

## we should migrate towards this setup...
: ${MY_ENVFILE:=/etc/default/mygw}
##: ${MY_LIB_COMMON:=/lib/mygw/common.sh}
##: ${MY_LIB_BIND:=/lib/mygw/my-bind.sh}

function panic {
	echo "####" >&2
	echo "#### ERROR: $1" >&2
	echo "####" >&2
	exit 1
}

function warn {
	local _rc=$?
	echo "###" >&2
	echo "### WARNING: $1" >&2
	echo "###" >&2
	return ${_rc}
}

function info {
	local _rc=$?
	echo "## INFO: $1" >&2
	return ${_rc}
}

function debug {
	local _rc=$?
	[ -n "${MY_DEBUG:-}" ] && echo "## DEBUG: $1" >&2
	return ${_rc}
}

function get_var_string {
	## still having issues on this one. for some reason it overrides the original \$1 parameter and everything goes to shit. this is to blame on debconf I believe
	local _my_var=$1
	local _my_value
	local _my_debconf_path="shared/mygw"		## if you change this path it also needs to be updated in the debian template file
	local _my_debconf_var="${_my_var,,}"
	local _bashset=$-

	_my_var=${!_my_var:-}

	if [ -z "${_my_var}" ] && [ -n "${MY_ENVFILE:-}" ]; then		## only do this if MY_ENVFILE var is actually set otherwise the grep and pipes below won't work
		set +u
		. /usr/share/debconf/confmodule

		db_register ${_my_debconf_path}/template_string ${_my_debconf_path}/${_my_debconf_var}
		db_subst ${_my_debconf_path}/${_my_debconf_var} myvar ${_my_var}
		db_fset ${_my_debconf_path}/${_my_debconf_var} seen false
		db_input high ${_my_debconf_path}/${_my_debconf_var}
		db_go
		db_get ${_my_debconf_path}/${_my_debconf_var}
		_my_value=$RET
		eval ${_my_var}='"${_my_value}"'		## be sure to use use single quotes as the variable needs to be quoted inside the eval as well to avoid problems on vars with spaces and other chars

		set -${_bashset}
		echo "## ${_my_var} set to ${_my_value}"
		grep -Eq "^ *${_my_var}=" ${MY_ENVFILE}   ||   echo "${_my_var}=" >> ${MY_ENVFILE}
		sed -i -e "s|^ *${_my_var}=.*|${_my_var}=${_my_value}|" ${MY_ENVFILE}
	fi
	return 0
}

function get_kernel_option {
	local _opt
	local _file
	local _value

	_file=/proc/cmdline
	_opt=${1:-}
	[ -z "${_opt}" ] && debug "no kernel option to retrieve given" && return 1

	if grep -q "${_opt}=\"" ${_file}; then
		_value=$(cat ${_file} | sed "s|.*${_opt}=\"\([^\"]*\).*|\1|")
	elif grep -q "${_opt}='" ${_file}; then
		_value=$(cat ${_file} | sed "s|.*${_opt}='\([^']*\).*|\1|")
	elif grep -q "${_opt}=" ${_file}; then
		_value=$(cat ${_file} | sed "s|.*${_opt}=\([^[:blank:]]*\).*|\1|")
	else
		debug "variable ${_opt} not found" || /bin/true
		return 1
	fi

	[ -z "${_value}" ] && debug "failed to get value" && return 1
	[ "${_value}" == "$(cat ${_file})" ] && debug "failed to get value" && return 1

	echo "${_value}"
	return 0
}

function get_my_tftp_server {
	local _kernelopt
	local _uplink
	local _gw6

	## first priority kernel command line option
	_kernelopt=$(get_kernel_option MY_TFTP_SERVER) || /bin/true
	if [ -n "${_kernelopt}" ]; then
		debug "using tftp server: ${_kernelopt}"
		echo "${_kernelopt}"
		return 0
	fi

	## next priority mygw config file
	if [ -n "${MY_TFTP_SERVER:-}" ]; then
		debug "using tftp server: ${MY_TFTP_SERVER}"
		echo "${MY_TFTP_SERVER}"
		return 0
	fi

	## last try to autodetect
	_gw6=$(get_my_gateway_ip6) || debug "failed to get ipv6 gateway" || return 1
	if [[ "${_gw6}" == fe80::* ]]; then
		_uplink=$(get_my_uplink_ip6_nic) || debug "failed to find matching uplink" || return 1
		echo "${_gw6}%${_uplink}"
		return 0
	else
		echo "${_gw6}"
		return 0
	fi

	return 1
}

function get_ifname_from_mac {
	local _mac="${1}"
	ip -br link | grep ${_mac} | awk '{ print $1 }' | sed 's/@.*//' || panic "nic with mac ${_mac} not found on this system"
}

function get_mac_from_ifname {
	local _ifname="${1}"
	local _mac=$(ip -o link show dev ${_ifname} | grep 'link/ether' | sed 's|.*link/ether \([^[:blank:]]*\).*|\1|') || debug "nic ${_ifname} not found" || return 1
	[ -z "${_mac}" ] && debug "unable to detect mac for nic ${_ifname}" && return 1
	echo "${_mac}"
}

function eui64 {
	local _mac="${1}"
	printf "%02x%s" $(( 16#${_mac:0:2} ^ 2#00000010 )) "${_mac:2}" \
		| sed -E -e 's/([0-9a-zA-Z]{2})*/0x\0|/g' \
		| tr -d ':\n' \
		| xargs -d '|' \
		printf "fe80::%02x%02x:%02xff:fe%02x:%02x%02x"
}

function gwmac {
	local _mac="${1}"
	printf "%02x%s" "0xfe" "${_mac:2}"
}

function testipv4 {
	local _ip4=${1:-}
	[[ ${_ip4} =~ ^[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}(/[0-9]{1,2}){0,1}$ ]] || debug "'${_ip4}' is not an IPv4" || return 1
}

function testipv6 {
	local _ip6=${1:-}
	[[ ${_ip6} =~ ^(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))(/[0-9]{1,3}){0,1}$ ]] || debug "'${_ip6}' is not an IPv6" || return 1
}

function get_ip4_from_nic {
	local _nic=${1}
	local _type=${2:-static}
	local _iponly=${3:-}
	local _ip
	case ${_type} in
		static)		_ip=$(ip -j addr show | jq -r '.[] | select(.ifname == "'"${_nic}"'") | .addr_info[] | first(select(.family == "inet" and .scope == "global" and (.dynamic | not))) | "\(.local)/\(.prefixlen)"') ;;
		dhcp)		_ip=$(ip -j addr show | jq -r '.[] | select(.ifname == "'"${_nic}"'") | .addr_info[] | first(select(.family == "inet" and .scope == "global" and .dynamic)) | "\(.local)/\(.prefixlen)"') ;;
		any)		_ip=$(ip -j addr show | jq -r '.[] | select(.ifname == "'"${_nic}"'") | .addr_info[] | first(select(.family == "inet" and .scope == "global")) | "\(.local)/\(.prefixlen)"') ;;
	esac
	testipv4 "${_ip}" || debug "no ipv4 for ${_nic} found" || return 1
	[ "${_iponly}" == "iponly" ] && echo "${_ip%/*}" && return 0
	echo "${_ip}"
}

function get_ip6_from_nic {
	local _nic=${1}
	local _type=${2:-static}
	local _class=${3:-global}
	local _iponly=${4:-}
	local _ip=""

	case ${_class} in
		global)	_class="2" ;;
		ula)	_class="fd" ;;
		any)	_class="" ;;
		*)		panic "unknown ip address class ${_class}" ;;
	esac

	case ${_type} in
		static)		_ip=$(ip -j addr show | jq -r '.[] | select(.ifname == "'"${_nic}"'") | .addr_info[] | first(select(.family == "inet6" and .scope == "global" and (.dynamic | not) and (.local | startswith("'${_class}'")))) | "\(.local)/\(.prefixlen)"') ;;
		dhcp)		_ip=$(ip -j addr show | jq -r '.[] | select(.ifname == "'"${_nic}"'") | .addr_info[] | first(select(.family == "inet6" and .scope == "global" and .dynamic and .prefixlen == 128 and (.local | startswith("'${_class}'")))) | "\(.local)/\(.prefixlen)"') ;;
		slaacok)	_ip=$(ip -j addr show | jq -r '.[] | select(.ifname == "'"${_nic}"'") | .addr_info[] | first(select(.family == "inet6" and .scope == "global" and .dynamic and .prefixlen != 128 and (.local | startswith("'${_class}'")))) | "\(.local)/\(.prefixlen)"') ;;

					## any only getting the first IP of any type of given class on that nic. needed by get_subnet6_from_nic in other words, multipls *subnets* on same nic are not supported (yet)
		any)		_ip=$(ip -j addr show | jq -r '.[] | select(.ifname == "'"${_nic}"'") | .addr_info[] | first(select(.family == "inet6" and .scope == "global" and (.local | startswith("'${_class}'")))) | "\(.local)/\(.prefixlen)"') ;;
		link|ll|linklocal)
					_ip=$(ip -j addr show | jq -r '.[] | select(.ifname == "'"${_nic}"'") | .addr_info[] | first(select(.family == "inet6" and .scope == "link")) | "\(.local)/\(.prefixlen)"') ;;
		*)			panic "unknown ip address type ${_type}" ;;
	esac

	testipv6 "${_ip}" || debug "no ${_class}/${_type} ipv6 for ${_nic} found" || return 1
	[ "${_iponly}" == "iponly" ] && echo "${_ip%/*}" && return 0
	echo "${_ip}"
}

function get_my_default_asn {
	local _ip
	IFS='./' read -r -a _ip <<< "$(get_ip4_from_nic lo static iponly || echo ${MY_NICS_LO_IPV4:-})"
	[ -z "${_ip:-}" ] && debug "unable to detect loopback ip" && return 1
	printf "4200%03d%03d\n" ${_ip[2]} ${_ip[3]}
}

function get_subnet4_from_nic {
	local _nic=${1}
	local _ip4
	_ip4=$(get_ip4_from_nic "${_nic}") || debug "unable to get ipv4 subnet from ${_nic}" || return 1
	echo $(sipcalc "${_ip4}" | grep "Network address" | sed 's/.*\t- //')/$(sipcalc ${_ip4} | grep "Network mask (bits)" | sed 's/.*- //')
}

function get_subnet6_from_nic {
	local _nic=${1}
	local _ip6
	local _net
	_ip6=$(get_ip6_from_nic "${_nic}" any global) || debug "unable to get ipv6 subnet from ${_nic}" || return 1
	_net=$(sipcalc "${_ip6}" | grep "Subnet prefix (masked)" | sed 's/.*\t- //')
	testipv6 ${_net} || debug "unable to get ipv6 subnet from ${_nic}" || return 1
	echo ${_net}
}

function get_my_public_ip4 {
	local _ip4
	_ip4=$(dig -4 +short myip.opendns.com @resolver1.opendns.com)
	testipv4 ${_ip4} || debug "unable to detect my public IPv4, using IPv6 only" || return 1
	echo ${_ip4}
}

function get_my_uplink_nic {
	local _nic
	_nic=$(ip -j route get 1 | jq -r '.[].dev')
	ip -o link show dev ${_nic} >/dev/null 2>&1 || debug "unable to detect nic" || return 1
	echo ${_nic}
}

function get_my_uplink_ip6_nic {
	local _nic
	_nic=$(ip -j route get 2000:: | jq -r '.[].dev')
	ip -o link show dev ${_nic} >/dev/null 2>&1 || debug "unable to detect nic" || return 1
	echo ${_nic}
}

function get_my_primary_ip {
	local _ip
	local _dst=${1}
	_ip=$(ip -j route get ${_dst} | jq -r '.[].prefsrc' || /bin/true)
	echo ${_ip}
}

function get_my_primary_ip4 {
	local _ip4
	local _dst=1
	_ip=$(get_my_primary_ip ${_dst})
	testipv4 ${_ip} || debug "unable to find my primary ipv4 address" || return 1
	echo ${_ip}
}

function get_my_primary_ip6 {
	local _ip
	local _dst=2000::
	_ip=$(get_my_primary_ip ${_dst})
	testipv6 ${_ip} || debug "unable to find my primary ipv6 address" || return 1
	echo ${_ip}
}

function get_my_gateway_ip6 {
	local _ip
	_ip=$(ip -j route get 2000:: | jq -r '.[].gateway' || /bin/true)
	testipv6 ${_ip} || debug "unable to find my ipv6 gateway address" || return 1
	echo ${_ip}
}

function get_l3_nics {
	local _nics
	#_nics=$(ip -6 -o addr show scope global permanent -noprefixroute | awk '{ print $2 }')		## only nics with static/permenent IPs
	_nics=$(ip -o addr show scope global | awk '{ print $2 }' | sort | uniq)				## incl all nics, inc those with only slaac/dhcp ips
	[ -z "${_nics}" ] && debug "no l3 nics found" && return 1
	echo ${_nics}
}

function get_my_nics {
	local _my_nics

	if [ -n "${MY_NICS:-}" ]; then
		debug "MY_NICS preset in config, no need to autodetect"
		echo "${MY_NICS}"
		return 0
	fi

	debug "MY_NICS not preset in config, trying to autodetect"
	_my_nics=$(grep "^MY_NICS_" ${MY_ENVFILE} 2>/dev/null | sed 's/^MY_NICS_\([[:alnum:]]*\)_.*/\1/' | sort | uniq | tr '[:upper:]' '[:lower:]')
	echo ${_my_nics}
}

function get_hostname {
	local _hostname
	_hostname=$(hostname)
	[ "${_hostname}" == "${_hostname/.}" ] && debug "not a fqdn hostname" && return 1
	echo ${_hostname}
}

function get_domainname {
	local _domainname
	[ -z "${_domainname:-}" ] && _domainname=$(cat /etc/resolv.conf | sed -e '/^domain /!d;s/^domain //')
	[ -z "${_domainname:-}" ] && _domainname=$(hostname -d)
	[ -z "${_domainname:-}" ] && debug "unable to get domain name" && return 1
	echo ${_domainname}
}

function get_subnet6_n {
	local _subnet=$1
	local _size=$2
	local _n=$3
	local _max_delta=${MY_IPV6_MAX_PLEN_DELTA}
	local _plen=${_subnet#*/}
	local _raw
	local _tmp_subnet

	debug "getting subnet ${_n} of size ${_size} in subnet ${_subnet}"

	if [ "${_plen}" -lt "$((${_size}-${_max_delta}))" ]; then
		debug "subnet size delta too big, breaking it up and recursing"
		_tmp_subnet=$(get_subnet6_n ${_subnet} $((${_size}-${_max_delta})) $((${_n} / (2**${_max_delta}) + 1))) || debug "unable to get temporary intermediate subnet" || return 1
		get_subnet6_n ${_tmp_subnet} ${_size} $((${_n} % (2**${_max_delta}))) || debug "unable to get ip" || return 1
		return 0
	fi

	_raw=$(sipcalc --v6split=/${_size} ${_subnet%/*}/${_plen} | grep "^Network" 2>/dev/null | head -n${_n} | tail -n1 | sed -e 's/.* \([0-9a-f:]\+\) .*/\1/')
	_raw=$(sipcalc ${_raw}/${_size} | grep "Compressed address" | sed -e 's/.* \([0-9a-f:]\+\)$/\1\/'${_size}'/')
	[ -z "${_raw}" ] && debug "unable to get subnet ${_n} of size ${_size} in subnet ${_subnet}" && return 1
	echo ${_raw}
}

function get_subnet4_n {
	local _subnet=$1
	local _size=$2
	local _n=$3
	local _raw=$(sipcalc --v4split=/${_size} ${_subnet} | grep "^Network" 2>/dev/null | sed -e 's/.* \(\([0-9]\{1,3\}\.*\)\{4\}\) .*/\1/' -e "${_n} !d")
	[ -z "${_raw}" ] && debug "unable to get the ${_n} subnet of size ${_size} in subnet ${_subnet}" && return 1
	_raw=${_raw}/${_size}
	testipv4 ${_raw} || debug "${_raw} seems not to be a valid ipv4 subnet" || return 1
	echo ${_raw}
}

function gen_resolv_conf {
	echo "domain ${MY_DOMAINNAME%.}"
	echo "search ${MY_DOMAINNAME%.}."
	## my own bind9 server
	if systemctl is-enabled bind9 >/dev/null 2>&1; then
		echo "nameserver ::1"
	## my own bind9 server - newer debian
	elif systemctl is-enabled named >/dev/null 2>&1; then
		echo "nameserver ::1"
	## default/configured DNS servers fallback
	elif [ -n "${MY_NAMESERVERS:-}" ]; then
		for dns in ${MY_NAMESERVERS//,/ }; do
			echo "nameserver ${dns}"
		done
	## I have ipv6 so use ipv6 dns
	elif [ -n "$(ip -6 route list match :: 2>/dev/null)" ] || [ -n "${MY_NAMESERVERS_IPV6_FORCE:-}" ]; then
		for dns in ${MY_NAMESERVERS_IPV6//,/ }; do
			echo "nameserver ${dns}"
		done
	## fallback to ipv4 name servers
	else
		for dns in ${MY_NAMESERVERS_IPV4//,/ }; do
			echo "nameserver ${dns}"
		done
	fi
}

function set_hostname {
	local _hostname=${MY_HOSTNAME}
	local _domainname=${MY_DOMAINNAME%.}

	## just a testing hook: simply return doing nothing if testing var is set to non-empty
	[ -n "${MY_HOSTNAME_TEST:-}" ] && echo ${_hostname}.${_domainname} && return 0

	[ -n "${_domainname}" ] && _hostname=${_hostname}.${_domainname}
	hostnamectl set-hostname ${_hostname} && return 0
	echo ${_hostname} >/etc/hostname
	hostname -F /etc/hostname
}

## generate ifupdown interfaces section for this interface
function gen_interface_config {
	local _nic=${1}
	local _mode=MY_NICS_${_nic^^}_MODE
	local _ip4=MY_NICS_${_nic^^}_IPV4
	local _ip6=MY_NICS_${_nic^^}_IPV6
	local _gw4=MY_NICS_${_nic^^}_GWV4
	local _gw6=MY_NICS_${_nic^^}_GWV6
	local _gwmac=MY_NICS_${_nic^^}_GWMAC
	local _mac=MY_NICS_${_nic^^}_MAC
	local _mtu=MY_NICS_${_nic^^}_MTU
	local _vlan=MY_NICS_${_nic^^}_VLAN
	local _ssid=MY_NICS_${_nic^^}_WIFI
	local _psk=MY_NICS_${_nic^^}_WIFI_PSK
	local _slaves=MY_NICS_${_nic^^}_SLAVES
	local _wifi_model=MY_NICS_${_nic^^}_WIFI_MODEL
	local _ip4_server=MY_NICS_${_nic^^}_IPV4_SERVER
	local _ap_config=MY_NICS_${_nic^^}_AP_CONFIG
	local _vrfid=MY_NICS_${_nic^^}_RTABLE
	local _vrf=MY_NICS_${_nic^^}_VRF
	local _dhcp_statless=0
	local _dhcp_pd=0
	local _gwmac
	local _gw4mac

	_mode=${!_mode:-}
	_ip4=${!_ip4:-}
	_ip6=${!_ip6:-manual}		## for protocol independent options I need it. ifupdown just wants the options in the first protocol which is ipv6
	_gw4=${!_gw4:-}
	_gw6=${!_gw6:-}
	_gwmac=${!_gwmac:-}
	_mac=${!_mac:-}
	_mtu=${!_mtu:-}
	_vlan=${!_vlan:-}
	_ssid=${!_ssid:-}
	_psk=${!_psk:-}
	_slaves=${!_slaves:-}
	_wifi_model=${!_wifi_model:-}
	_ip4_server=${!_ip4_server:-}
	_ap_config=${!_ap_config:-}
	_vrfid=${!_vrfid:-${MY_MGMTVRF_ID}}
	_vrf=${!_vrf:-${MY_MGMTVRF}}

	_mode=${_mode,,}			## make it all lowercase

	echo "## ${_nic} block ##"
	echo "auto ${_nic}"

	if [ "${_nic}" == "lo" ]; then
		echo "iface lo inet loopback"
	#else
	#	echo "allow-hotplug ${_nic}"
	fi

	## for ifupdown all additional "nic-wide" configs need to be part of whatever the first address family is
	## so I'm gonna go ahead and force IPv6 config, while IPv4 is optional
	## which is why IPv4 config is below all other addon configs, and IPv6 on top
	case "${_ip6}" in
		dhcp|dhcppd)
			[ "${_ip6}" == "dhcppd" ] && _dhcp_pd=1
			echo "iface ${_nic} inet6 dhcp"
			echo "    request_prefix ${_dhcp_pd:=0}"
			echo "    accept_ra 2"
			echo "    up bash -c 'sysctl net.ipv6.conf.\${IFACE/./\/}.accept_ra_rt_info_max_plen=128' || /bin/true"
			echo "    up bash -c 'sysctl net.ipv6.conf.\${IFACE/./\/}.accept_ra_rt_info_min_plen=${MY_DHCP6_PLEN}' || /bin/true"
			;;

		manual)
			echo "iface ${_nic} inet6 manual"
			;;

		unnumbered)
			if [ -z "${_mac}" ]; then
				_mac=$(get_mac_from_ifname ${_nic})
			fi

			if [ -z "${_gwmac}" ] && [ -z "${_gw6}" ]; then
				_gwmac=$(gwmac ${_mac})		## need to do this here otherwise exit code does not catch
			fi

			if [ -z "${_gw6}" ]; then
				_gw6=$(eui64 ${_gwmac})
			fi

			cat <<-EOF
				iface ${_nic} inet6 manual
				    up ip -6 route add default via ${_gw6} dev \${IFACE}
			EOF
			if [ -n "${_gwmac}" ]; then
				echo "    up ip -6 neighbor add ${_gw6} lladdr ${_gwmac} dev \${IFACE}"
			fi
			;;

		slaac|stateless|statelesspd)
			[ "${_ip6}" == "stateless" ] && _dhcp_statless=1
			[ "${_ip6}" == "statelesspd" ] && _dhcp_statless=1
			[ "${_ip6}" == "statelesspd" ] && _dhcp_pd=1
			echo "iface ${_nic} inet6 auto"
			echo "    dhcp ${_dhcp_statless:=0}"
			echo "    request_prefix ${_dhcp_pd:=0}"
			echo "    accept_ra 2"
			echo "    up bash -c 'sysctl net.ipv6.conf.\${IFACE/./\/}.accept_ra_rt_info_max_plen=128' || /bin/true"
			echo "    up bash -c 'sysctl net.ipv6.conf.\${IFACE/./\/}.accept_ra_rt_info_min_plen=${MY_DHCP6_PLEN}' || /bin/true"
			;;

		*)
			testipv6 "${_ip6}" || debug "unsupported ipv6 mode ${_ip6} for nic ${_nic}" || return 1

			if [ -n "${_ip4_server}" ]; then
				echo "iface ${_nic} inet6 v4tunnel"
			else
				echo "iface ${_nic} inet6 static"
				echo "    dad-attempts 0"
			fi
			echo "    address ${_ip6}"
			[ -n "${_gw6}" ] && echo "    gateway ${_gw6}"
			[ -n "${_ip4_server}" ] && echo "    endpoint ${_ip4_server}"
			echo "    accept_ra 2"
			echo "    up bash -c 'sysctl net.ipv6.conf.\${IFACE/./\/}.accept_ra_rt_info_max_plen=128' || /bin/true"
			echo "    up bash -c 'sysctl net.ipv6.conf.\${IFACE/./\/}.accept_ra_rt_info_min_plen=${MY_DHCP6_PLEN}' || /bin/true"
			[ -n "${_gw6}" ] && echo "    up bash -c 'sysctl net.ipv6.conf.\${IFACE/./\/}.accept_ra_defrtr=0'"
			;;
	esac

	## custom mtu?
	[ -n "${_mtu}" ] && echo "    mtu ${_mtu}"

	## weird shit we have to do on the r2, maybe/hopefully we only need it for this one
	if [ "${_wifi_model}" == "bpi-r2" ]; then
		echo "    pre-up /usr/sbin/init-r2-wifi"
	fi

	## special interface modes
	case "${_mode}" in
		mgmt)
			debug "vrf enabled. services need to be bound to the vrf specifically to respond. alternatively (but discouraged) set net.ipv4.tcp_l3mdev_accept=1 to globally ignore vrf on wildcard socket binds. you should be knowing what you're doing"

			cat <<-EOF
			    pre-up /bin/ip link add ${_vrf} type vrf table ${_vrfid}
			    pre-up /bin/ip link set up dev ${_vrf}
			    pre-up /bin/ip link set master ${_vrf} dev ${_nic}
			    up service sshd@${_vrf} start &
			    down service sshd@${_vrf} stop
			    post-down /bin/ip link del dev ${_vrf}
			EOF
	;;
	esac

	## is the nic supposed to be a wifi client?
	if [ -n "${_ssid}" ]; then
		cat <<-EOF
		    wpa-ssid "${_ssid}"
		    wpa-psk "${_psk}"
		EOF
	fi

	## is the nic supposed to be a wifi AP?
	[ -n "${_ap_config}" ] && echo "    hostapd ${_ap_config}"

	## is this a virtual bridge interface? if so add neccessary stuff
	if [ -n "${_slaves}" ]; then

		## do we have trunk ports part of the bridge - this is slightly different from vlan section below which is more intended for native l3 trunk interfaces..... this can probably be improved
		for _sl in ${_slaves}; do
			if [[ ${_sl} == *.* ]]; then
				echo "    pre-up /bin/ip link add link ${_sl%.*} name ${_sl} type vlan id ${_sl#*.} loose_binding on"
				echo "    post-down /bin/ip link del dev ${_sl}"
			fi
		done

		cat <<-EOF
		    bridge_ports ${_slaves}
		    bridge_stp no
		    up /bin/ip link set dev \${IFACE} type bridge mcast_snooping 0
		EOF
	fi

	## is the nic supposed to be a vlan
	if [ -n "${_vlan}" ]; then
		echo "    pre-up /bin/ip link add link ${_vlan%.*} name \${IFACE} type vlan id ${_vlan#*.} loose_binding on"
		echo "    post-down /bin/ip link del dev \${IFACE}"
	fi

	## for ifupdown all additional "nic-wide" configs need to be part of whatever the first address family is
	## so I'm gonna go ahead and force IPv6 config, while IPv4 is optional
	## which is why IPv4 config is below all other addon configs, and IPv6 on top
	case "${_ip4}" in
		dhcp)
			echo "iface ${_nic} inet dhcp"
			;;

		manual)
			echo "iface ${_nic} inet manual"
			;;

		unnumbered)
			if [ -z "${_mac}" ]; then
				_mac=$(get_mac_from_ifname ${_nic})
			fi

			if [ -z "${_gwmac}" ] && [ -z "${_gw6}" ]; then
				_gwmac=$(gwmac ${_mac})		## need to do this here otherwise exit code does not catch
			fi

			if [ -z "${_gw6}" ]; then
				_gw6=$(eui64 ${_gwmac})
			fi

			cat <<-EOF
				iface ${_nic} inet manual
				    up ip -4 route add default via inet6 ${_gw6} dev \${IFACE}
				    up bash -c 'sysctl -w net.ipv4.conf.\${IFACE/./\/}.arp_ignore=8'
			EOF
			;;

		"")
			debug "no IPv4 defined for ${_nic}"
			;;

		*)
			testipv4 "${_ip4}" || debug "unsupported ipv4 mode ${_ip4} for nic ${_nic}" || return 1
			echo "iface ${_nic} inet static"
			echo "    address ${_ip4}"
			[ -n "${_gw4}" ] && echo "    gateway ${_gw4}"
			;;
	esac

	echo
	echo
}

function gen_net_udev_rule {
	local _nic=${1}
	local _os_name
	local _mac=MY_NICS_${_nic^^}_MAC
	_mac=${!_mac:-}

	[ -z "${_mac}" ] && debug "no mac set for ${_nic}, assuming its virtual or we re just using the system default name" && return 0

	cat <<-EOF
		SUBSYSTEM=="net", ACTION=="add", DRIVERS=="?*", ATTR{address}=="${_mac}", ATTR{type}=="1", NAME="${_nic}"
	EOF
}

function get_subnet6_range {
	## probably only needed for dhcpd6 pd config
	local _subnet=$1
	local _size=${2#/}
	local _start=${3:-1}
	local _end=${4:-'$'}
	local _max_delta=${MY_IPV6_MAX_PLEN_DELTA}

	## it makes no sense to cut off such a big pie into such small pieces.
	_plen=${_subnet#*/}
	[ "${_plen}" -lt "$((${_size}-${_max_delta}))" ] && _plen=$((${_size}-${_max_delta})) && info "too big size delta (>${_max_delta}) of supernet (${_subnet}) and subnet (/${_size}), decreasing supernet to /${_plen}."

	local _raw=$(sipcalc --v6split=/${_size} ${_subnet%/*}/${_plen} | grep "^Network" 2>/dev/null | sed -e 's/.* \([0-9a-f:]\+\) .*/\1\/'${_size}'/' -e ${_start}b -e "${_end}!d" | xargs sipcalc | grep "Compressed address" | sed 's/.*\t- //')
	[ -z "${_raw}" ] && debug "unable to get range (${_start}-${_end}) of size ${_size} in subnet ${_subnet}" && return 1
	echo ${_raw} | grep -q " " || debug "unable to get range (${_start}-${_end}) of size ${_size} in subnet ${_subnet}" || return 1
	echo ${_raw}
}

function get_ipv4_nameserver_for_nic {
	local _nic=$1
	local _nameserver=MY_NICS_${_nic^^}_NAMESERVER_IPV4

	## if I have a hardcoded IPv4 name server for this specific NIC than thats got the highest priority
	_nameserver=${!_nameserver:-}
	[ -n "${_nameserver}" ] && echo "${_nameserver}" && debug "ipv4 nameserver ${_nameserver} set from nic specific config" && return 0

	## if I have my own nameserver, just use that, regardless of ipv4/6 situation
	systemctl is-enabled bind9 >/dev/null 2>&1 && get_my_primary_ip4 && debug "running a local nameserver using my own primary IP" && return 0
	systemctl is-enabled named >/dev/null 2>&1 && get_my_primary_ip4 && debug "running a local nameserver using my own primary IP" && return 0

	## if I have ipv6 on this nic screw DNS4 all together. this helps for nameserver that are reachable on IPv6 only. as they would get wronly negative cache if querried over ipv4
	[ -z "$(ip -o -6 addr show scope global dev ${_nic})" ] && debug "looks like no own DNS server and working IPv6 setup. so not providing IPv4." && return 1

	## looks like I have no dedicated dns, nor run my own, nor IPv6 at all. so falling back to my default IPv4 dns from config (*if* that even exists)
	[ -z "${MY_NAMESERVERS_IPV4}" ] && debug "I got nothing to show for a IPv4 DNS server" && return 1
	echo ${MY_NAMESERVERS_IPV4}
}

function get_ipv6_nameserver_for_nic {
	local _nic=$1
	local _nameserver=MY_NICS_${_nic^^}_NAMESERVER_IPV6
	_nameserver=${!_nameserver:-}

	[ -z "${_nameserver}" ] && systemctl is-enabled bind9 >/dev/null 2>&1 && _nameserver=$(get_my_primary_ip6)			## if I have my own nameserver, just use that, regardless of ipv4/6 situation
	[ -z "${_nameserver}" ] && systemctl is-enabled named >/dev/null 2>&1 && _nameserver=$(get_my_primary_ip6)			## if I have my own nameserver, just use that, regardless of ipv4/6 situation
	[ -z "${_nameserver}" ] && [ -z "$(ip -o -4 addr show dev ${_nic})" ] && _nameserver=${MY_NAMESERVERS_DNS64}		## if I have ipv6 only use dns64
	[ -z "${_nameserver}" ] && _nameserver=${MY_NAMESERVERS_IPV6}														## looks like I have no dedicated dns, nor run my own, and I have dual stack. so gonna use regular ipv6 nameservers

	echo "${_nameserver}"
}

function gen_dhcpd6_subnet_from_nic {
	local _nic=$1
	local _range_pd=MY_NICS_${_nic^^}_IPV6_DHCPPD
	local _domainname=MY_NICS_${_nic^^}_DOMAINNAME
	local _range_bitlen=${MY_DHCP6_PLEN}					## just break any IPv6 stuff into /96. to be used in small environments.
	local _subnet6
	local _range
	local _range_temp

	_range_pd="${!_range_pd:-}"
	_domainname=${!_domainname:-${MY_DOMAINNAME}}

	_subnet6="$(get_subnet6_from_nic ${_nic})" || return 1
	_range=$(get_subnet6_n ${_subnet6} ${_range_bitlen} 1) || return 1			## break subnet6 and get first /96 for stateful leases
	_range_temp=$(get_subnet6_n ${_subnet6} ${_range_bitlen} 2) || return 1		## break subnet6 and get second /96 for privacy/temp leases

	[ -n "${_range_pd}" ] && _range_pd="$(get_subnet6_range ${_range_pd} ${_range_bitlen} 17) /${_range_bitlen}"

	_nameserver=$(get_ipv6_nameserver_for_nic ${_nic})
	_nameserver=${_nameserver// /,}

	echo "subnet6 ${_subnet6} {"
	echo "    range6 ${_range};"
	echo "    range6 ${_range_temp} temporary;"
	[ -n "${_range_pd}" ] && echo "    prefix6 ${_range_pd};"
	echo "    option dhcp6.domain-search \"${_domainname}\";"
	echo "    option dhcp6.name-servers ${_nameserver};"
	echo "}"
}

function gen_dhcpd4_subnet_from_nic {
	local _nic=$1
	local _start=50
	local _end=150
	local _gw4
	local _subnet4
	local _mask4
	local _nodns4
	local _nameserver
	local _domainname=MY_NICS_${_nic^^}_DOMAINNAME

	_domainname=${!_domainname:-${MY_DOMAINNAME}}

	_gw4=$(get_ip4_from_nic "${_nic}") || return 1
	_subnet4=$(get_subnet4_from_nic "${_nic}") || return 1
	_mask4=$(sipcalc "${_subnet4}" | grep -P "Network mask\t" | sed 's/.*\t- //') || return 1

	_nameserver=$(get_ipv4_nameserver_for_nic ${_nic}) || _nodns4=true
	_nameserver=${_nameserver// /,}

	echo "subnet ${_subnet4%/*} netmask ${_mask4} {"
	echo "    range ${_subnet4%.*}.${_start} ${_subnet4%.*}.${_end};"
	echo "    option routers ${_gw4%/*};"
	echo "    option domain-name \"${_domainname}\";"
	[ -z "${_nodns4:-}" ] && echo "    option domain-name-servers ${_nameserver};"
	echo "}"
}

function get_ipv6_only_nics {
	diff --new-line-format="" --unchanged-line-format="" <(ip -o -6 addr show scope global | awk '{ print $2 }' | sort) <(ip -o -4 addr show scope global | awk '{ print $2 }' | sort)
}

function get_vrf_ifs {
	local _vrf=${1:-${MY_MGMTVRF}}
	ip -br link show master ${_vrf} | awk '{ print $1 }' | xargs echo || debug "unable to detect mgmt ifs, maybe they don't exist?"
}

function get_vrfs {
	local _vrfs
	_vrfs="$(ip -br link show type vrf | awk '{ print $1 }' | xargs echo)"
	[ -z "${_vrfs}" ] && debug "unable to detect vrfs, maybe they don't exist?" && return 1
	echo "${_vrfs}"
}

function get_oem_string {
	local _string
	_string=$(dmidecode --oem-string ${1:-1}) || debug "unable to get oem-string"
	[ -z "${_string}" ] && debug "oem string empty/not provided" && return 1
	_string=$(base64 -d <<<"${_string}") || debug "oem string is not a valid base64 encoding" || return 1
	echo "${_string}" || return 1
	return 0
}
